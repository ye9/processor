module decoder_2_4(out, in);

    input [1:0] in;
    output [3:0] out;

    wire [1:0] not_in;
    not not_in_0(not_in[0], in[0]);
    not not_in_1(not_in[1], in[1]);

    and out_0(out[0], not_in[1], not_in[0]);
    and out_1(out[1], not_in[1], in[0]);
    and out_2(out[2], in[1], not_in[0]);
    and out_3(out[3], in[1], in[0]);

endmodule
