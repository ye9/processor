module decoder_5_32(out, in);

    input [4:0] in;
    output [31:0] out;

    wire [3:0] decoder_2_4_output;

    decoder_2_4 decoder_2_4_1(decoder_2_4_output, in[4:3]);

    decoder_3_8 decoder_3_8_1(out[7:0], in[2:0], decoder_2_4_output[0]);
    decoder_3_8 decoder_3_8_2(out[15:8], in[2:0], decoder_2_4_output[1]);
    decoder_3_8 decoder_3_8_3(out[23:16], in[2:0], decoder_2_4_output[2]);
    decoder_3_8 decoder_3_8_4(out[31:24], in[2:0], decoder_2_4_output[3]);

endmodule
