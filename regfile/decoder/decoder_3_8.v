module decoder_3_8(out, in, en);

    input [2:0] in;
    input en;
    output [7:0] out;

    wire [2:0] not_in;
    not not_in_0(not_in[0], in[0]);
    not not_in_1(not_in[1], in[1]);
    not not_in_2(not_in[2], in[2]);

    and out_0(out[0], not_in[2], not_in[1], not_in[0], en);
    and out_1(out[1], not_in[2], not_in[1], in[0], en);
    and out_2(out[2], not_in[2], in[1], not_in[0], en);
    and out_3(out[3], not_in[2], in[1], in[0], en);
    and out_4(out[4], in[2], not_in[1], not_in[0], en);
    and out_5(out[5], in[2], not_in[1], in[0], en);
    and out_6(out[6], in[2], in[1], not_in[0], en);
    and out_7(out[7], in[2], in[1], in[0], en);

endmodule
