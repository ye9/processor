# Processor Checkpoint: RegFile
---

#### Decoder
- I built a 5-to-32 bit decoder using one 2-to-4 bit decoder and four 3-to-8 bit decoders
  - This helped to reduce the overall complexity of the 5-to-32 bit decoder

#### Register
- I built a 32-bit register using a *genvar* construct *for* loop and 32 D-Flip-Flops

#### Regfile
- I passed *ctrl_writeReg*, *ctrl_readRegA*, and *ctrl_readRegB* each into a 5-to-32 bit decoder. I then took the decoded bits from *ctrl_writeReg* and used them to create 32 registers, with register 0 having write enable disabled. I then used tri-state buffers to map the outputs of the 32 registers to either register A or register B depending on the decoded bits from *ctrl_readRegA*, and *ctrl_readRegB*.
