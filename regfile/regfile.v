module regfile (
	clock,
	ctrl_writeEnable, ctrl_reset, ctrl_writeReg,
	ctrl_readRegA, ctrl_readRegB, data_writeReg,
	data_readRegA, data_readRegB
);

		input clock, ctrl_writeEnable, ctrl_reset;
		input [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
		input [31:0] data_writeReg;

		output [31:0] data_readRegA, data_readRegB;

		wire [31:0] write_reg_decoder_output, read_reg_a_decoder_output, read_reg_b_decoder_output;
		decoder_5_32 write_reg_decoder(write_reg_decoder_output, ctrl_writeReg);
		decoder_5_32 read_reg_a_decoder(read_reg_a_decoder_output, ctrl_readRegA);
		decoder_5_32 read_reg_b_decoder(read_reg_b_decoder_output, ctrl_readRegB);

		//Register 0
		wire [31:0] register_0_output;
		register_32 reg_32_0(register_0_output, data_writeReg, clock, ctrl_reset, 1'b0);
		tri_state read_reg_a_tri_state(data_readRegA, register_0_output, read_reg_a_decoder_output[0]);
		tri_state read_reg_b_tri_state(data_readRegB, register_0_output, read_reg_b_decoder_output[0]);

		genvar i;
		generate
			for (i = 1; i < 32; i = i + 1) begin
				wire en;
				wire [31:0] register_output;
				and enable(en, write_reg_decoder_output[i], ctrl_writeEnable);
				register_32 reg_32(register_output, data_writeReg, clock, ctrl_reset, en);

				tri_state read_reg_a_tri_state(data_readRegA, register_output, read_reg_a_decoder_output[i]);
				tri_state read_reg_b_tri_state(data_readRegB, register_output, read_reg_b_decoder_output[i]);
			end
		endgenerate

endmodule
