module overflow_detection(ovf, sign_A, sign_B, sign_S);

    input sign_A, sign_B, sign_S;
    output ovf;

    wire not_sign_A, not_sign_B, not_sign_S;
    not not_sign_of_A(not_sign_A, sign_A);
    not not_sign_of_B(not_sign_B, sign_B);
    not not_sign_of_S(not_sign_S, sign_S);

    wire [1:0] overflow_terms;
    and overflow_term_1(overflow_terms[0], not_sign_A, not_sign_B, sign_S);
    and overflow_term_2(overflow_terms[1], sign_A, sign_B, not_sign_S);
    or overflow_result(ovf, overflow_terms[0], overflow_terms[1]);

endmodule
