# Processor Checkpoint: ALU
---

#### CLA
- I used a 1-bit full adder in which I calculated the sum as well as *g* and *p* for two inputs
- I used the 1-bit full adder to make an 8-bit carry-lookahead adder in which I calculated *G* and *P* (although I could have used my bitwise ***and*** and ***or*** functions for this)
- I then cascaded my 8-bit adders to form a 32-bit carry-lookahead adder

#### Barrel Shifter
- For both my ***sll*** and my ***sra*** functions, I first created 1-bit, 2-bit, 4-bit, 8-bit, and 16-bit shifters using *genvar constructs*
- I then used muxes to determine which of these shifts to apply based off of the *shiftamt*

#### Output Signals
- For my ***overflow*** signal, I calculated both an *add_overflow* and a *subtract_overflow* and used a 2-1 mux to determine which to return based off of the *ctrl_ALUopcode*
- For my ***isNotEqual*** signal, I created a *is_not_zero* module where I passed all of the bits of *data_result* into an *or* gate. This could be done as the signal only had to be true after a subtraction operation
- For my ***isLessThan*** signal, as the signal only had to be true after a subtraction operation I determined that *A* is less than *B* in only two cases:
    -  When *A* is positive and subtraction results in overflow
    -  When *A* is negative and subtraction does not result in overflow

Thus, this allowed me to simply *xor* the most significant bit of *data_result* and the ***overflow*** detection signal
