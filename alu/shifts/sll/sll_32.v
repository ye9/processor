module sll_32(out, A, shiftamt);

    input [31:0] A;
    input [4:0] shiftamt;
    output [31:0] out;

    wire [31:0] mux_output_0, mux_output_1, mux_output_2, mux_output_3;
    wire [31:0] shift_output_0, shift_output_1, shift_output_2, shift_output_3, shift_output_4;

    left_shift_16 shift_16(shift_output_0, A);
    mux_2 shift_16_mux(mux_output_0, shiftamt[4], A, shift_output_0);

    left_shift_8 shift_8(shift_output_1, mux_output_0);
    mux_2 shift_8_mux(mux_output_1, shiftamt[3], mux_output_0, shift_output_1);

    left_shift_4 shift_4(shift_output_2, mux_output_1);
    mux_2 shift_4_mux(mux_output_2, shiftamt[2], mux_output_1, shift_output_2);

    left_shift_2 shift_2(shift_output_3, mux_output_2);
    mux_2 shift_2_mux(mux_output_3, shiftamt[1], mux_output_2, shift_output_3);

    left_shift_1 shift_1(shift_output_4, mux_output_3);
    mux_2 shift_1_mux(out, shiftamt[0], mux_output_3, shift_output_4);

endmodule
