module left_shift_4 (out, in);

    input [31:0] in;
    output [31:0] out;

    genvar i;
    generate
      for (i = 4; i < 32; i = i + 1) begin
        assign out[i] = in[i-4];
      end
    endgenerate

    assign out[0] = 1'b0;
    assign out[1] = 1'b0;
    assign out[2] = 1'b0;
    assign out[3] = 1'b0;

endmodule
