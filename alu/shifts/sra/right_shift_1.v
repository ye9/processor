module right_shift_1 (out, in);

    input [31:0] in;
    output [31:0] out;

    wire MSB;
    assign MSB = in[31];

    genvar i;
    generate
      for (i = 0; i < 31; i = i + 1) begin
        assign out[i] = in[i+1];
      end
    endgenerate

    assign out[31] = MSB;

endmodule
