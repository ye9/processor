module right_shift_2 (out, in);

    input [31:0] in;
    output [31:0] out;

    wire MSB;
    assign MSB = in[31];

    genvar i;
    generate
      for (i = 0; i < 30; i = i + 1) begin
        assign out[i] = in[i+2];
      end
    endgenerate

    assign out[31] = MSB;
    assign out[30] = MSB;

endmodule
