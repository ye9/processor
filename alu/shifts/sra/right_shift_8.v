module right_shift_8 (out, in);

    input [31:0] in;
    output [31:0] out;

    wire MSB;
    assign MSB = in[31];

    genvar i;
    generate
      for (i = 0; i < 24; i = i + 1) begin
        assign out[i] = in[i+8];
      end
    endgenerate

    assign out[31] = MSB;
    assign out[30] = MSB;
    assign out[29] = MSB;
    assign out[28] = MSB;
    assign out[27] = MSB;
    assign out[26] = MSB;
    assign out[25] = MSB;
    assign out[24] = MSB;

endmodule
