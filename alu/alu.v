module alu(data_operandA, data_operandB, ctrl_ALUopcode, ctrl_shiftamt, data_result, isNotEqual, isLessThan, overflow);

    input [31:0] data_operandA, data_operandB;
    input [4:0] ctrl_ALUopcode, ctrl_shiftamt;

    output [31:0] data_result;
    output isNotEqual, isLessThan, overflow;

    wire [31:0] add_result, subtract_result, and_result, or_result, sll_result, sra_result;
    wire Cout_add, Cout_subtract;

    wire add_overflow, subtract_overflow;

    // add
    cla_32 add_operation(add_result, Cout_add, add_overflow, data_operandA, data_operandB, 1'b0);

    // subtract
    wire [31:0] not_data_operandB;
    not_bitwise not_B_subtract(not_data_operandB, data_operandB);
    cla_32 subtract_operation(subtract_result, Cout_subtract, subtract_overflow, data_operandA, not_data_operandB, 1'b1);

    // and
    and_bitwise and_operation(and_result, data_operandA, data_operandB);

    // or
    or_bitwise or_operation(or_result, data_operandA, data_operandB);

    // sll
    sll_32 sll(sll_result, data_operandA, ctrl_shiftamt);

    // sra
    sra_32 sra(sra_result, data_operandA, ctrl_shiftamt);

    mux_8 opcode_mux(data_result, ctrl_ALUopcode[2:0], add_result, subtract_result, and_result, or_result, sll_result, sra_result, 32'd0, 32'd0);

    assign overflow = ctrl_ALUopcode[0] ? subtract_overflow : add_overflow;

    is_not_zero equality(isNotEqual, data_result);

    xor comparison(isLessThan, data_result[31], overflow);

endmodule
