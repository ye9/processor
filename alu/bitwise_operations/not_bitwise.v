module not_bitwise(out, A);
    input [31:0] A;
    output [31:0] out;

    genvar i;
    generate
      for (i = 0; i < 32; i = i + 1) begin
        not not_bit(out[i], A[i]);
      end
    endgenerate

endmodule
