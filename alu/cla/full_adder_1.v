module full_adder_1(g, p, S, A, B, Cin);

	input A, B, Cin;
	output S, p, g;

	xor Sresult(S, A, B, Cin);

	and gen(g, A, B);
	or prop(p, A, B);

endmodule
