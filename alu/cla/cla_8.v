module cla_8(G, P, S, A, B, Cin);

    input Cin;
    input [7:0] A, B;

    output G, P;
    output [7:0] S;

    wire [7:0] g, p;
    wire [7:0] carry;

    full_adder_1 adder_0(g[0], p[0], S[0], A[0], B[0], Cin);
    wire carry_1;
    and carry_1_and(carry_1, p[0], Cin);
    or carry_1_or(carry[0], g[0], carry_1);


    full_adder_1 adder_1(g[1], p[1], S[1], A[1], B[1], carry[0]);
    wire [1:0] carry_2;
    and carry_2_and_0(carry_2[0], p[1], g[0]);
    and carry_2_and_1(carry_2[1], p[1], p[0], Cin);
    or carry_2_or(carry[1], g[1], carry_2[0], carry_2[1]);


    full_adder_1 adder_2(g[2], p[2], S[2], A[2], B[2], carry[1]);
    wire [2:0] carry_3;
    and carry_3_and_0(carry_3[0], p[2], p[1], g[0]);
    and carry_3_and_1(carry_3[1], p[2], p[1], p[0], Cin);
    and carry_3_and_2(carry_3[2], p[2], g[1]);
    or carry_3_or(carry[2], g[2], carry_3[0], carry_3[1], carry_3[2]);


    full_adder_1 adder_3(g[3], p[3], S[3], A[3], B[3], carry[2]);
    wire [3:0] carry_4;
    and carry_4_and_0(carry_4[0], p[3], p[2], p[1], g[0]);
    and carry_4_and_1(carry_4[1], p[3], p[2], p[1], p[0], Cin);
    and carry_4_and_2(carry_4[2], p[3], p[2], g[1]);
    and carry_4_and_3(carry_4[3], p[3], g[2]);
    or carry_4_or(carry[3], g[3], carry_4[0], carry_4[1], carry_4[2], carry_4[3]);


    full_adder_1 adder_4(g[4], p[4], S[4], A[4], B[4], carry[3]);
    wire [4:0] carry_5;
    and carry_5_and_0(carry_5[0], p[4], p[3], p[2], p[1], g[0]);
    and carry_5_and_1(carry_5[1], p[4], p[3], p[2], p[1], p[0], Cin);
    and carry_5_and_2(carry_5[2], p[4], p[3], p[2], g[1]);
    and carry_5_and_3(carry_5[3], p[4], p[3], g[2]);
    and carry_5_and_4(carry_5[4], p[4], g[3]);
    or carry_5_or(carry[4], g[4], carry_5[0], carry_5[1], carry_5[2], carry_5[3], carry_5[4]);


    full_adder_1 adder_5(g[5], p[5], S[5], A[5], B[5], carry[4]);
    wire [5:0] carry_6;
    and carry_6_and_0(carry_6[0], p[5], p[4], p[3], p[2], p[1], g[0]);
    and carry_6_and_1(carry_6[1], p[5], p[4], p[3], p[2], p[1], p[0], Cin);
    and carry_6_and_2(carry_6[2], p[5], p[4], p[3], p[2], g[1]);
    and carry_6_and_3(carry_6[3], p[5], p[4], p[3], g[2]);
    and carry_6_and_4(carry_6[4], p[5], p[4], g[3]);
    and carry_6_and_5(carry_6[5], p[5], g[4]);
    or carry_6_or(carry[5], g[5], carry_6[0], carry_6[1], carry_6[2], carry_6[3], carry_6[4], carry_6[5]);


    full_adder_1 adder_6(g[6], p[6], S[6], A[6], B[6], carry[5]);
    wire [6:0] carry_7;
    and carry_7_and_0(carry_7[0], p[6], p[5], p[4], p[3], p[2], p[1], g[0]);
    and carry_7_and_1(carry_7[1], p[6], p[5], p[4], p[3], p[2], p[1], p[0], Cin);
    and carry_7_and_2(carry_7[2], p[6], p[5], p[4], p[3], p[2], g[1]);
    and carry_7_and_3(carry_7[3], p[6], p[5], p[4], p[3], g[2]);
    and carry_7_and_4(carry_7[4], p[6], p[5], p[4], g[3]);
    and carry_7_and_5(carry_7[5], p[6], p[5], g[4]);
    and carry_7_and_6(carry_7[6], p[6], g[5]);
    or carry_7_or(carry[6], g[6], carry_7[0], carry_7[1], carry_7[2], carry_7[3], carry_7[4], carry_7[5], carry_7[6]);


    full_adder_1 adder_7(g[7], p[7], S[7], A[7], B[7], carry[6]);
    wire [7:0] carry_8;
    and carry_8_and_0(carry_8[0], p[7], p[6], p[5], p[4], p[3], p[2], p[1], g[0]);
    and carry_8_and_1(carry_8[1], p[7], p[6], p[5], p[4], p[3], p[2], p[1], p[0], Cin);
    and carry_8_and_2(carry_8[2], p[7], p[6], p[5], p[4], p[3], p[2], g[1]);
    and carry_8_and_3(carry_8[3], p[7], p[6], p[5], p[4], p[3], g[2]);
    and carry_8_and_4(carry_8[4], p[7], p[6], p[5], p[4], g[3]);
    and carry_8_and_5(carry_8[5], p[7], p[6], p[5], g[4]);
    and carry_8_and_6(carry_8[6], p[7], p[6], g[5]);
    and carry_8_and_7(carry_8[7], p[7], g[6]);
    or carry_8_or(carry[7], g[7], carry_8[0], carry_8[1], carry_8[2], carry_8[3], carry_8[4], carry_8[5], carry_8[6], carry_8[7]);


    and block_propagate(P, p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7]);

    wire [6:0] gen;
    and block_generate_0(gen[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], g[0]);
    and block_generate_1(gen[1], p[2], p[3], p[4], p[5], p[6], p[7], g[1]);
    and block_generate_2(gen[2], p[3], p[4], p[5], p[6], p[7], g[2]);
    and block_generate_3(gen[3], p[4], p[5], p[6], p[7], g[3]);
    and block_generate_4(gen[4], p[5], p[6], p[7], g[4]);
    and block_generate_5(gen[5], p[6], p[7], g[5]);
    and block_generate_6(gen[6], p[7], g[6]);
    or block_generate(G, g[7], gen[0], gen[1], gen[2], gen[3], gen[4], gen[5], gen[6]);

endmodule
