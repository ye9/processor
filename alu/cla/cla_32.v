module cla_32(S, Cout, overflow, A, B, Cin);

    input [31:0] A, B;
    input Cin;
    output [31:0] S;
    output Cout, overflow;

    wire [3:0] G, P;

    cla_8 cla_8_0(G[0], P[0], S[7:0], A[7:0], B[7:0], Cin);
    wire c_8, c_8_term;
    and c_8_and(c_8_term, P[0], Cin);
    or c_8_or(c_8, G[0], c_8_term);

    cla_8 cla_8_1(G[1], P[1], S[15:8], A[15:8], B[15:8], c_8);
    wire c_16;
    wire [1:0] c_16_terms;
    and c_16_and_1(c_16_terms[0], P[1], G[0]);
    and c_16_and_2(c_16_terms[1], P[1], P[0], Cin);
    or c_16_or(c_16, G[1], c_16_terms[0], c_16_terms[1]);

    cla_8 cla_8_2(G[2], P[2], S[23:16], A[23:16], B[23:16], c_16);
    wire c_24;
    wire [2:0] c_24_terms;
    and c_24_and_1(c_24_terms[0], P[2], G[1]);
    and c_24_and_2(c_24_terms[1], P[2], P[1], G[0]);
    and c_24_and_3(c_24_terms[2], P[2], P[1], P[0], Cin);
    or c_24_or(c_24, G[2], c_24_terms[0], c_24_terms[1], c_24_terms[2]);


    cla_8 cla_8_3(G[3], P[3], S[31:24], A[31:24], B[31:24], c_24);
    wire [3:0] c_32_terms;
    and c_32_and_1(c_32_terms[0], P[3], G[2]);
    and c_32_and_2(c_32_terms[1], P[3], P[2], G[1]);
    and c_32_and_3(c_32_terms[2], P[3], P[2], P[1], G[0]);
    and c_32_and_4(c_32_terms[3], P[3], P[2], P[1], P[0], Cin);
    or c_32_or(Cout, G[3], c_32_terms[0], c_32_terms[1], c_32_terms[2], c_32_terms[3]);

    overflow_detection ovf(overflow, A[31], B[31], S[31]);

endmodule
