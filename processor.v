/**
 * READ THIS DESCRIPTION!
 *
 * This is your processor module that will contain the bulk of your code submission. You are to implement
 * a 5-stage pipelined processor in this module, accounting for hazards and implementing bypasses as
 * necessary.
 *
 * Ultimately, your processor will be tested by a master skeleton, so the
 * testbench can see which controls signal you active when. Therefore, there needs to be a way to
 * "inject" imem, dmem, and regfile interfaces from some external controller module. The skeleton
 * file, Wrapper.v, acts as a small wrapper around your processor for this purpose. Refer to Wrapper.v
 * for more details.
 *
 * As a result, this module will NOT contain the RegFile nor the memory modules. Study the inputs
 * very carefully - the RegFile-related I/Os are merely signals to be sent to the RegFile instantiated
 * in your Wrapper module. This is the same for your memory elements.
 *
 *
 */
module processor(
    // Control signals
    clock,                          // I: The master clock
    reset,                          // I: A reset signal

    // Imem
    address_imem,                   // O: The address of the data to get from imem
    q_imem,                         // I: The data from imem

    // Dmem
    address_dmem,                   // O: The address of the data to get or put from/to dmem
    data,                           // O: The data to write to dmem
    wren,                           // O: Write enable for dmem
    q_dmem,                         // I: The data from dmem

    // Regfile
    ctrl_writeEnable,               // O: Write enable for RegFile
    ctrl_writeReg,                  // O: Register to write to in RegFile
    ctrl_readRegA,                  // O: Register to read from port A of RegFile
    ctrl_readRegB,                  // O: Register to read from port B of RegFile
    data_writeReg,                  // O: Data to write to for RegFile
    data_readRegA,                  // I: Data from port A of RegFile
    data_readRegB                   // I: Data from port B of RegFile

	);

	// Control signals
	input clock, reset;

	// Imem
  output [31:0] address_imem;
	input [31:0] q_imem;

	// Dmem
	output [31:0] address_dmem, data;
	output wren;
	input [31:0] q_dmem;

	// Regfile
	output ctrl_writeEnable;
	output [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	output [31:0] data_writeReg;
	input [31:0] data_readRegA, data_readRegB;

	/* YOUR CODE STARTS HERE */

  wire [31:0] pc_out, pc_in, pc_plus_1;
  wire stall;
  pc_latch pc(pc_out, pc_in, clock, reset, !stall);

  // pc + 1
  wire Cout, pc_overflow;
  cla_32 pc_plus1(pc_plus_1, Cout, pc_overflow, pc_out, 32'b1, 1'b0);

  assign address_imem = pc_out;

  // ******* FD LATCH *******
  wire [31:0] fd_pc_out, fd_ir_out;
  // replace with branch jumping later
  fd_latch fd(fd_pc_out, fd_ir_out, pc_plus_1, q_imem, clock, reset, !stall);

  // ******* DATA STAGE *******
  assign ctrl_readRegA = fd_ir_out[21:17];
  assign ctrl_readRegB = fd_ir_out[16:12];

  // ******* DX LATCH *******
  wire [31:0] dx_pc_out, dx_a_out, dx_b_out, dx_ir_out, dx_ir_in;
  assign dx_ir_in = stall ? 32'd0 : fd_ir_out;
  dx_latch dx(dx_pc_out, dx_a_out, dx_b_out, dx_ir_out, fd_pc_out, data_readRegA, data_readRegB, dx_ir_in, clock, reset);

  // ******* EXECUTE STAGE *******
  wire [31:0] alu_a, xm_o_out;
  wire [1:0] alu_a_in_bypass_select;
  mux_4 alu_a_in_bypass(alu_a, alu_a_in_bypass_select, xm_o_out, data_writeReg, dx_a_out, 32'b0);

  wire [31:0] xm_b_in;
  wire [1:0] alu_b_in_bypass_select;
  mux_4 alu_b_in_bypass(xm_b_in, alu_b_in_bypass_select, xm_o_out, data_writeReg, dx_b_out, 32'b0);

  wire [4:0] execute_opcode;
  assign execute_opcode = dx_ir_out[31:27];

  // i-type instructions: addi, sw, lw, bne, blt
  wire is_i_type;
  assign is_i_type = execute_opcode == 5'b00101 || execute_opcode == 5'b00111 || execute_opcode == 5'b01000 || execute_opcode == 5'b00010 || execute_opcode == 5'b00110;

  wire [31:0] sx_dx_imm;
  assign sx_dx_imm = {{16{dx_ir_out[16]}}, dx_ir_out[15:0]};

  wire [31:0] alu_b;
  assign alu_b = is_i_type ? sx_dx_imm : xm_b_in;

  wire [4:0] alu_opcode, shamt;
  assign alu_opcode = is_i_type ? 5'b0 : dx_ir_out[6:2];
  assign shamt = dx_ir_out[11:7];

  wire [31:0] xm_o_in;
  wire isNotEqual, isLessThan, overflow;

  alu alu_execute(alu_a, alu_b, alu_opcode, shamt, xm_o_in, isNotEqual, isLessThan, overflow);

  // ******* XM LATCH *******
  wire [31:0] xm_b_out, xm_ir_out;
  xm_latch xm(xm_o_out, xm_b_out, xm_ir_out, xm_o_in, xm_b_in, dx_ir_out, clock, reset);

  // ******* MEMORY STAGE *******
  assign address_dmem = xm_o_out;
  wire wm_bypass_select;
  assign data = wm_bypass_select ? data_writeReg : xm_b_out;
  wire [4:0] memory_opcode;
  assign memory_opcode = xm_ir_out[31:27];
  assign wren = memory_opcode == 5'b00111;


  // ******* MW LATCH *******
  wire [31:0] mw_o_out, mw_d_out, mw_ir_out;
  mw_latch mw(mw_o_out, mw_d_out, mw_ir_out, xm_o_out, q_dmem, dx_ir_out, clock, reset);

  // ******* WRITEBACK STAGE *******
  assign ctrl_writeReg = mw_ir_out[26:22];
  wire [4:0] writeback_opcode;
  assign writeback_opcode = xm_ir_out[31:27];
  assign data_writeReg = writeback_opcode == 5'b01000 ? mw_d_out : mw_o_out;

  wire addi_or_setx_writeback, alu_or_lw_writeback, jal_writeback;
  assign addi_or_setx_writeback = !writeback_opcode[3] && writeback_opcode[2] && !writeback_opcode[1] && writeback_opcode[0];
  assign alu_or_lw_writeback = !writeback_opcode[4] && !writeback_opcode[2] && !writeback_opcode[1] && !writeback_opcode[0];
  assign jal_writeback = !writeback_opcode[5] && !writeback_opcode[4] && !writeback_opcode[2] && writeback_opcode[1] && writeback_opcode[0];

  assign ctrl_writeEnable = addi_or_setx_writeback || alu_or_lw_writeback || jal_writeback;


  // ******* BYPASSING *******
  wire [4:0] dx_ir_rs1, xm_ir_rd, mw_ir_rd;
  assign dx_ir_rs1 = dx_ir_out[21:17];
  assign xm_ir_rd = xm_ir_out[26:22];
  assign mw_ir_rd = mw_ir_out[26:22];

  assign alu_a_in_bypass_select[1] = (dx_ir_rs1 != xm_ir_rd) && (dx_ir_rs1 != mw_ir_rd);
  assign alu_a_in_bypass_select[0] = dx_ir_rs1 == mw_ir_rd || alu_a_in_bypass_select[1];


  wire [4:0] dx_ir_rs2;
  assign dx_ir_rs2 = dx_ir_out[16:12];

  assign alu_b_in_bypass_select[1] = (dx_ir_rs2 != xm_ir_rd) && (dx_ir_rs2 != mw_ir_rd);
  assign alu_b_in_bypass_select[0] = dx_ir_rs2 == mw_ir_rd || alu_b_in_bypass_select[1];

  assign wm_bypass_select = (xm_ir_rd == mw_ir_rd) && (writeback_opcode == 5'b00111);

  // ******* STALLING *******
  wire [4:0] fd_ir_rs1, fd_ir_rs2, dx_ir_rd;
  assign fd_ir_rs1 = fd_ir_out[21:17];
  assign fd_ir_rs2 = fd_ir_out[16:12];
  assign dx_ir_rd = dx_ir_out[26:22];


  assign stall = (execute_opcode == 5'b01000) && (fd_ir_rs1 == dx_ir_rd) && (fd_ir_rs2 == dx_ir_rd) && (fd_ir_out[31:27] != 5'b00111);

	/* END CODE */

endmodule
