module register_32(out, in, clock, reset, en);

    input [31:0] in;
    input clock, reset, en;
    output [31:0] out;

    genvar i;
    generate
      for (i = 0; i < 32; i = i + 1) begin
        dffe_ref dffe(out[i], in[i], clock, en, reset);
      end
    endgenerate

endmodule
