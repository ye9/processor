module register_65(out, in, clock, reset, en);

    input [64:0] in;
    input clock, reset, en;
    output [64:0] out;

    genvar i;
    generate
      for (i = 0; i < 65; i = i + 1) begin
        dffe_ref dffe(out[i], in[i], clock, en, reset);
      end
    endgenerate

endmodule
