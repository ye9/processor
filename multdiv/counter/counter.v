module counter(count, clock, reset);

    input clock, reset;
    output [31:0] count;

    wire [31:0] sum;
    register_32 count_reg(count, sum, clock, reset, 1'b1);

    wire Cout, overflow;
    cla_32 adder(sum, Cout, overflow, count, 32'b1, 1'b0);

endmodule
