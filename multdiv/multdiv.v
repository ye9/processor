module multdiv(
	data_operandA, data_operandB,
	ctrl_MULT, ctrl_DIV,
	clock,
	data_result, data_exception, data_resultRDY);

    input [31:0] data_operandA, data_operandB;
    input ctrl_MULT, ctrl_DIV, clock;

    output [31:0] data_result;
    output data_exception, data_resultRDY;

		wire [31:0] mult_result, div_result;
		wire mult_exception, mult_resultRDY, div_exception, div_resultRDY;

		wire is_multiply;
		sr_latch latch(ctrl_MULT, ctrl_DIV, is_multiply);

		multiply_32 multiply(mult_result, mult_exception, mult_resultRDY, data_operandA, data_operandB, ctrl_MULT, clock);
		divide_32 divide(div_result, div_exception, div_resultRDY, data_operandA, data_operandB, ctrl_DIV, clock);

	 	assign data_result    = is_multiply ?  mult_result : div_result;
	 	assign data_resultRDY = is_multiply ? mult_resultRDY : div_resultRDY;
	 	assign data_exception = is_multiply ? mult_exception : div_exception;

endmodule
