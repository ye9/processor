# Processor Checkpoint: MultDiv
---

- I used an SR-latch in order to determine when either *ctrl_MULT* or *ctrl_DIV* were asserted, effectively returning the appropriate results when either was triggered.

#### Multiplier
- I used the modified Booth's algorithm in order to determine the result.
- I used a counter to keep track of the step that I was currently on.
- I used a control to determine what to do given the lower 3 bits of my register.

#### Divider
- I used the Non-Restoring Division algorithm in order to determine the result.
- I determined the two's complement of the dividend, divisor, and result, ultimately utilizing these in the case that the signs of the operands differed.
