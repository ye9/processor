module control(do_nothing, shift, add_sub, multiplier_bits);

    input [2:0] multiplier_bits;
    output do_nothing, shift, add_sub;

    wire [31:0] booth_mux_output;
    mux_8 modified_booth(booth_mux_output, multiplier_bits, 32'b001, 32'b000, 32'b000, 32'b010, 32'b110, 32'b100, 32'b100, 32'b001);

    assign do_nothing = booth_mux_output[0];
    assign shift = booth_mux_output[1];
    assign add_sub = booth_mux_output[2];

endmodule
