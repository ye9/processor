module multiply_32(data_result, data_exception, data_resultRDY, multiplicand, multiplier, ctrl_MULT, clock);

    input [31:0] multiplicand, multiplier;
    input ctrl_MULT, clock;

    output [31:0] data_result;
    output data_exception, data_resultRDY;


    wire do_nothing, shift, add_sub;
    control modified_booth(do_nothing, shift, add_sub, current_multiplier[2:0]);

    wire [31:0] shifted_multiplicand;
    assign shifted_multiplicand = multiplicand << 1;

    wire [31:0] booth_steps [2:0];
    mux_2 shift_check(booth_steps[0], shift, multiplicand, shifted_multiplicand);

    wire [31:0] not_shifted_multiplicand;
    assign not_shifted_multiplicand = ~booth_steps[0];
    mux_2 add_sub_check(booth_steps[1], add_sub, booth_steps[0], not_shifted_multiplicand);

    mux_2 do_nothing_check(booth_steps[2], do_nothing, booth_steps[1], 32'b0);

    wire [31:0] product, booth_sum;
    wire Cout, overflow;
    cla_32 booth_adder(booth_sum, Cout, overflow, product, booth_steps[2], add_sub);


    wire [64:0] initial_product_multiplier;
    assign initial_product_multiplier[0] = 1'b0;
    assign initial_product_multiplier[32:1] = multiplier;
    assign initial_product_multiplier[64:33] = 32'b0;


    wire signed [64:0] product_multiplier;
    wire [32:0] current_multiplier;

    assign product_multiplier = {booth_sum, current_multiplier};

    wire signed [64:0] shifted_product_multiplier;
    assign shifted_product_multiplier = product_multiplier >>> 2;

    wire signed [64:0] register_input;
    mux_2_65 initialize_register(register_input, step_zero, shifted_product_multiplier, initial_product_multiplier);

    wire signed [64:0] register_output;
    register_65 register(register_output, register_input, clock, ctrl_MULT, 1'b1);

    assign current_multiplier = register_output[32:0];

    assign product = register_output[64:33];

    wire [31:0] count;
    counter step_counter(count, clock, ctrl_MULT);
    assign step_zero = ~|count[4:0];


    assign data_result = register_output[32:1];
    assign data_resultRDY = !(count[4:0] ^ 5'b10001);

    wire sign_check;
    assign sign_check = (multiplicand[31] ^ multiplier[31]) ^ register_output[32];
    assign data_exception = (sign_check & |multiplicand & |multiplier) | ((|register_output[64:32]) & (|(~register_output[64:32])));

endmodule
