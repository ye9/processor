module divide_32(data_result, data_exception, data_resultRDY, dividend, divisor, ctrl_DIV, clock);

    input [31:0] dividend, divisor;
    input ctrl_DIV, clock;

    output [31:0] data_result;
    output data_exception, data_resultRDY;

    wire signed [31:0] Q;
    wire [31:0] dividend_complement, divisor_complement, result_complement;
    twos_complement_32 twos_comp_dividend(dividend_complement, dividend);
    twos_complement_32 twos_comp_divisor(divisor_complement, divisor);
    twos_complement_32 twos_comp_result(result_complement, Q);

    wire [31:0] sign_adjusted_dividend, sign_adjusted_divisor, sign_adjusted_result;

    wire different_signs;
    assign different_signs = dividend[31] ^ divisor[31];

    assign sign_adjusted_dividend = dividend[31] ? dividend_complement : dividend;
    assign sign_adjusted_divisor = divisor[31] ? divisor_complement : divisor;

    wire signed [31:0] NRDI_step_1;
    wire signed [31:0] A;
    mux_2 sign_A_check_1(NRDI_step_1, A[31], ~sign_adjusted_divisor, sign_adjusted_divisor);


    wire [31:0] NRDI_sum;
    wire Cout_1, overflow_1;

    cla_32 NRDI_adder(NRDI_sum, Cout_1, overflow_1, current_A_Q[63:32], NRDI_step_1, !A[31]);

    wire signed [31:0] A_sum;
    wire signed final_restore;
    and final_restore_check(final_restore, A[31], data_resultRDY);
    mux_2 initialize_A(A_sum, final_restore, NRDI_sum, current_A_Q[63:32]);

    wire signed [31:0] current_Q;
    wire signed [63:0] register_output;

    mux_2 initialize_Q(current_Q, ctrl_DIV, current_A_Q[31:0], sign_adjusted_dividend);


    register_32 A_reg(A, A_sum, clock, ctrl_DIV, 1'd1);
    register_32 Q_reg(Q, current_Q, clock, 1'd0, 1'd1);

    wire signed [63:0] A_Q;
    assign A_Q = {A, Q};
    wire signed [63:0] shifted_A_Q;
    assign shifted_A_Q = A_Q << 1;

    wire signed [63:0] current_A_Q;
    assign current_A_Q = {shifted_A_Q[63:1], !A[31]};


    wire [31:0] count;
    counter step_counter(count, clock, ctrl_DIV);
    assign step_zero = ~|count[5:0];


    assign data_result = different_signs ? result_complement : Q;
    assign data_resultRDY = !(count[5:0] ^ 6'b100001);


    assign data_exception = !(divisor ^ 32'd0);

endmodule
