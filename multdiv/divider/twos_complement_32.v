module twos_complement_32(out, in);

    input [31:0] in;
    output [31:0] out;

    wire Cout, overflow;
    cla_32 complement_adder(out, Cout, overflow, ~in, 32'd1, 1'b0);

endmodule
