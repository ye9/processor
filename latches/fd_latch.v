module fd_latch(pc_out, ir_out, pc_in, ir_in, clock, reset, en);

    input [31:0] pc_in, ir_in;
    input clock, reset, en;
    output [31:0] pc_out, ir_out;

    register_32 pc(pc_out, pc_in, clock, en, reset);
    register_32 ir(ir_out, ir_in, clock, en, reset);

endmodule
