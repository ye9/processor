module dx_latch(pc_out, a_out, b_out, ir_out, pc_in, a_in, b_in, ir_in, clock, reset);

    input [31:0] pc_in, a_in, b_in, ir_in;
    input clock, reset;
    output [31:0] pc_out, a_out, b_out, ir_out;

    register_32 pc(pc_out, pc_in, clock, 1'b1, reset);
    register_32 a(a_out, a_in, clock, 1'b1, reset);
    register_32 b(b_out, b_in, clock, 1'b1, reset);
    register_32 ir(ir_out, ir_in, clock, 1'b1, reset);

endmodule
