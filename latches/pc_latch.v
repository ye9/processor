module pc_latch(out, in, clock, reset, en);

    input [31:0] in;
    input clock, reset, en;
    output [31:0] out;

    register_32 pc(out, in, clock, reset, en);

endmodule
