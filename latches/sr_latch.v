module sr_latch(A, B, state);

    input A, B;
    output state;

    assign state = (A ~| state) ~| B;

endmodule
