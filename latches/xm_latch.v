module xm_latch(o_out, b_out, ir_out, o_in, b_in, ir_in, clock, reset);

    input [31:0] o_in, b_in, ir_in;
    input clock, reset;
    output [31:0] o_out, b_out, ir_out;

    register_32 o(o_out, o_in, clock, 1'b1, reset);
    register_32 b(b_out, b_in, clock, 1'b1, reset);
    register_32 ir(ir_out, ir_in, clock, 1'b1, reset);

endmodule
